#
# Docker image to build XELK 4.x.x
#
FROM ubuntu:16.04

MAINTAINER DAVE Embedded Systems (devel@dave.eu)

# install required build packages
RUN apt-get update && \
	apt-get install -y build-essential git-core gcc-arm-none-eabi \
		flex bison vim curl libc6-dev-i386 bc ccache lzop \
		diffstat unzip texinfo gcc-multilib chrpath socat \
		sudo lintian && \
	apt-get install -y u-boot-tools && \
	apt-get install -y gawk wget python && \
	echo "dash dash/sh boolean false" | debconf-set-selections && \
	DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# get Yocto toolchain
RUN wget --no-verbose https://mirror.dave.eu/axel/xelk-4.0.0-rc0/xelk-4.0.0_sdk.sh -O sdk.sh && \
	chmod +x sdk.sh && \
	mkdir /toolchain && \
	./sdk.sh -d /toolchain -y && \
	rm sdk.sh
COPY env.sh /toolchain/env.sh

RUN mkdir /build
WORKDIR /build

COPY docker_entrypoint.sh /root/docker_entrypoint.sh
ENTRYPOINT ["/root/docker_entrypoint.sh"]
